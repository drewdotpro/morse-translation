"use strict"
// We need one library here, that's packaged in Node, so no package.json required yet
const fs = require('fs')

/**
 * findWords - recursive function to find if there is a word that the phrase starts with or continues with
 * @param remainingMorsePhrase {string} the phrase that's left to be translated (shrinks as we recurse)
 * @param wordsAndPhrases {{phrases: string[], words: string[] wordsAsMorse[]}} stateful object of found phrases, dictionary (words) and dictionary as morse
 * @param phraseSoFar {string[]} the words we've managed to match in a chain so far towards a possible complete phrase
 */
function findWords(remainingMorsePhrase, wordsAndPhrases, phraseSoFar = []) {
  wordsAndPhrases.wordsAsMorse.forEach((word, idx) => {
    // If phrase starts with this word let's continue, otherwise we'll drop off here.
    if (remainingMorsePhrase.startsWith(word)) {
      // words and wordsAsMorse have matched indexes
      const foundWord = wordsAndPhrases.words[idx]
      // We have found this word, so we can strip if off the front of our phrase and only leave what follows
      const newRemainingMorsePhrase = remainingMorsePhrase.substring(word.length)
      // If there's nothing left, we've reached the end of the word
      if (newRemainingMorsePhrase.length === 0) {
        // Join an array of the phrase with spaces so it's a sentence
        wordsAndPhrases.phrases.push([...phraseSoFar, foundWord].join(' '))
      } else {
        // There's still characters left in the morse phrase so let's keep checking to see if we can match it.
        findWords(newRemainingMorsePhrase, wordsAndPhrases, [...phraseSoFar, foundWord])
      }
    }
  })
}

/**
 * translate - build our morse version of the dictionary and begin iterating
 * @param sentence {string} The morse code sentence for translation
 * @param providedWords {string[]} Array of words in our dictionary
 */
function translate(sentence, providedWords) {
  const morseMap = {
    'a': '.-',
    'b': '-...',
    'c': '-.-.',
    'd': '-..',
    'e': '.',
    'f': '..-.',
    'g': '--.',
    'h': '....',
    'i': '..',
    'j': '.---',
    'k': '-.-',
    'l': '.-..',
    'm': '--',
    'n': '-.',
    'o': '---',
    'p': '.--.',
    'q': '--.-',
    'r': '.-.',
    's': '...',
    't': '-',
    'u': '..-',
    'v': '...-',
    'w': '.--',
    'x': '-..-',
    'y': '-.--',
    'z': '--..',
  }
  //Map over our words and create the morse version
  const wordsAsMorse = providedWords.map((word) => {
    const arrayOfMorse = [...word].map((letter) => morseMap[letter])
    return arrayOfMorse.join('')
  })
  // This object can get passed through iterations so it is stateful for this call but not a risky global
  const wordsAndPhrases = {
    phrases: [],
    words: providedWords,
    wordsAsMorse,
  }
  // Recursive call to iterate over the sentence options
  findWords(sentence, wordsAndPhrases)

  // Did we match?
  if (wordsAndPhrases.phrases.length === 0) {
    // No matches
    console.log('No phrases were found for that morse code')
  } else {
    // Matches
    console.log('The following phrases were matched:')
    wordsAndPhrases.phrases.forEach(phrase => console.log(phrase))
  }
}

/**
 * start - parse command line arguments and validate their content
 * Use defaults when no arguments are provided
 * Validate the contents of dictionary files
 */
function start() {
  // Default sentence if not in arg values
  let sentence = '-....-.--.-.....-...-.--'
  // Default path to dictionary if not in arg values
  let dictionaryPath = './data/google1000.txt'

  // Did the user provide custom morse code?
  if (process.argv.length >= 3) {
    console.log('Reading Command Line Morse Code...')
    if (/[-.]+$/.test(process.argv[2]) !== true) {
      console.log('The Morse Code you provide may only be - and . characters')
      process.exit(1)
    }
    sentence = process.argv[2]
  }

  // Did the user provide custom dictionary path?
  if (process.argv.length >= 4) {
    console.log('Reading Command Line Path for dictionary...')
    dictionaryPath = process.argv[3]
  }

  // Attempt to read the dictionary file
  let dictionary
  try {
    dictionary = fs.readFileSync(dictionaryPath, 'utf-8').split('\n').filter(word => word !== '')
  } catch (e) {
    console.error('Could not read dictionary file', e)
    process.exit(1)
  }
  // Validate the dictionary is only a-z words we're expecting in our morse code
  dictionary.forEach((word) => {
    if (/^[a-z]+$/.test(word) === false) {
      console.error(`Words may only be lowercase a-z split by newline, found ${word}`)
      process.exit(1)
    }
  })

  // Begin translation
  translate(sentence, dictionary)
}

// Begin the application
start()
