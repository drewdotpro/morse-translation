## Technical Assessment - Morse Translation


### Requirements

Create a function that allows the specification of a joined morse code phrase defined as `-` and `.` characters and a dictionary of words, to determine the possible phrase.


### Usage

This code was written against NodeJS v10.15.0, it is recommended to be run with the same

#### Basic use

```
node morse
```

This will run the application with the default sentence `-....-.--.-.....-...-.--` and the default dictionary path of `./data/google1000.txt`

#### Command Arguments

```
node morse --..-...--.-------... ./data/some-other-file.txt
```

The first argument you may specify your own morse code, the second you may specify the relative path to a dictionary file.


Thank you,

Drew Llewellyn
